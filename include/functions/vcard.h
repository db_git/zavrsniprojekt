#ifndef FUNCTIONS_VCARD_H
#define FUNCTIONS_VCARD_H 1

#include "definitions/int.h"
#include "definitions/restrict.h"
#include "definitions/struct.h"

void export_vcf(struct Kontakt * restrict_ptr const * restrict_ptr,
						const int_u32_t);

#endif /* FUNCTIONS_VCARD_H */
