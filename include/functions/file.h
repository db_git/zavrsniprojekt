#ifndef FUNCTIONS_FILE_H
#define FUNCTIONS_FILE_H 1

#include "definitions/bool.h"
#include "definitions/int.h"
#include "definitions/restrict.h"
#include "definitions/struct.h"

void check_empty_file(bool_t * restrict_ptr const);
struct Kontakt **ucitaj_kontakte(int_u32_t * restrict_ptr const);
void spremi_kontakte(struct Kontakt * restrict_ptr const * restrict_ptr const,
							const int_u32_t);

#endif /* FUNCTIONS_FILE_H */
