#ifndef LIBFUNC_STRTOK_H
#define LIBFUNC_STRTOK_H

#include "definitions/char.h"
#include "definitions/restrict.h"

char_t *str_token(register char_t * restrict_ptr,
		register const char_t * restrict_ptr const,
		register char_t * restrict_ptr * restrict_ptr const);

#endif /* LIBFUNC_STRTOK_H */
