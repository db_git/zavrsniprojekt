#ifndef DEFINITIONS_BOOL_H
#define DEFINITIONS_BOOL_H 1

#ifdef HAS_STDBOOL_H

#include <stdbool.h>

#define FALSE_DEF false
#define TRUE_DEF true
typedef bool bool_t;

#else

#include "definitions/int.h"

#define FALSE_DEF 0
#define TRUE_DEF 1
typedef int_u8_t bool_t;

#endif /* HAS_STDBOOL_H */

#endif /* DEFINITIONS_BOOL_H */
