#ifndef DEFINITIONS_INT_H
#define DEFINITIONS_INT_H 1

#include "definitions/os.h"
#include "definitions/wide.h"

#ifdef HAS_INTTYPES_H
#include <inttypes.h>
#endif /* HAS_INTTYPES_H */

#ifdef HAS_WCHAR_H

#ifdef HAS_INTTYPES_H
#define PR_U16 WIDE("%") WIDE(PRIuFAST16)
#define PR_U32 WIDE("%") WIDE(PRIuFAST32)
#else
#define PR_U16 L"%u"
#define PR_U32 L"%u"
#endif /* HAS_INTTYPES_H */

#else

#ifdef HAS_INTTYPES_H
#define PR_U16 "%" PRIuFAST16
#define PR_U32 "%" PRIuFAST32
#else
#define PR_U16 "%u"
#define PR_U32 "%u"
#endif /* HAS_INTTYPES_H */

#endif /* HAS_WCHAR_H */


#ifdef HAS_INTTYPES_H

typedef uint_fast8_t int_u8_t;
typedef uint_fast16_t int_u16_t;
typedef uint_fast32_t int_u32_t;
typedef int_fast64_t int_s64_t;
typedef uint_fast64_t int_u64_t;

#else

typedef unsigned short int_u8_t;
typedef unsigned int int_u16_t;
typedef unsigned int int_u32_t;
typedef signed long int_s64_t;

#if (OS_TYPE == 32)
typedef unsigned long long int_u64_t;
#else
typedef unsigned long int_u64_t;
#endif /* OS_TYPE */

#endif /* HAS_INTTYPES_H */

#endif /* DEFINITIONS_INT_H */
