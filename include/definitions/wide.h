#ifndef DEFINITIONS_WIDE_H
#define DEFINITIONS_WIDE_H

/*
 * This is required for compilers that can't properly concatenate
 * L"a" and "b" into L"ab" when using wide chars (i.e. MSVC)
 */
#ifdef HAS_WCHAR_H
#define _WIDEN_(x) L ## x
#define WIDE(x) _WIDEN_(x)
#else
#define WIDE(x) x
#endif /* HAS_WCHAR_H */

#endif /* DEFINITIONS_WIDE_H */
