#ifndef DEFINITIONS_INLINE_H
#define DEFINITIONS_INLINE_H 1

/*
 * While the compiler is smarter about which functions to
 * inline we still force inlining of small and static
 * functions, others are left to the compiler to choose
 */
#if defined(__GNUC__) || defined(__TINYC__)
	#if defined(__STDC_VERSION__) && ((__STDC_VERSION__) >= (199901L))
		#define inline_func inline __attribute__((__always_inline__))
	#else
		#define inline_func __attribute__((__always_inline__))
	#endif /* __STDC_VERSION__ */

#elif (defined(__clang__) || defined(__CLANG__))
	#if defined(__STDC_VERSION__) && ((__STDC_VERSION__) >= (199901L))
		#if __has_attribute(__always_inline__)
			#define inline_func inline __attribute__((__always_inline__))
		#else
			#define inline_func inline
		#endif /* __has_attribute(__always_inline__) */
	#else
		#if __has_attribute(__always_inline__)
			#define inline_func __attribute__((__always_inline__))
		#else
			#define inline_func
		#endif /* __has_attribute(__always_inline__) */
	#endif /* __STDC_VERSION__ */

#elif defined(_MSC_VER) || defined(_MSC_FULL_VER) || defined(_MSC_BUILD)
	#define inline_func inline __forceinline

#elif defined(__STDC_VERSION__) && ((__STDC_VERSION__) >= (199901L))
	#define inline_func inline

#else
	#define inline_func

#endif /* __GNUC__, __clang__, __CLANG__, __STDC_VERSION__,
 	_MSC_VER, _MSC_FULL_VER, _MSC_BUILD */

#endif /* DEFINITIONS_INLINE_H */
