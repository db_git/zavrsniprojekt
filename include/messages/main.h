#ifndef MESSAGES_MAIN_H
#define MESSAGES_MAIN_H 1

#define MSG_NO_EXPORT "Nema kontakata za izvoz!\n"
#define MSG_NO_SAVE "Nema kontakata za spremanje!\n"
#define MSG_NO_DELETE "Nema kontakata za brisanje!\n"
#define MSG_NO_PRINT "Nema kontakata za ispis!\n"
#define MSG_NO_SORT "Nema kontakata za sortiranje!\n"
#define MSG_SORTED "Kontakti sortirani!\n"
#define MSG_SAVED "Kontakti spremljeni!\n"
#define MSG_WRONG_CHOICE "Krivi odabir!\n"

#ifdef HAS_WCHAR_H
#define MSG_MENU_MAIN L"Unesite:\n\n"					\
			L"1) Dodaj kontakt\n"				\
			L"2) Uredi kontakt\n"				\
			L"3) Obri" L"\u0161" L"i kontakt\n"		\
			L"4) Ispis kontakata\n"				\
			L"5) Sortiranje kontakata\n"			\
			L"6) Izvoz kontakata\n"				\
			L"7) Spremi kontakte\n"				\
			L"0) Iza" L"\u0111" L"i iz programa\n\n"

#define MSG_EXIT L"Jeste li sigurni da "		\
		L"\u017e" L"elite iza" L"\u0107"	\
		L"i iz programa?\n\n[Da / Ne]: "

#define MSG_NO_EDIT L"Nema kontakata za ure" L"\u0111" L"ivanje!\n"
#else
#define MSG_MENU_MAIN "Unesite:\n\n"				\
		"1) Dodaj kontakt\n"				\
		"2) Uredi kontakt\n"				\
		"3) Obrisi kontakt\n"				\
		"4) Ispis kontakata\n"				\
		"5) Sortiranje kontakata\n"			\
		"6) Izvoz kontakata\n"				\
		"7) Spremi kontakte\n"				\
		"0) Izadi iz programa\n\n"

#define MSG_EXIT "Jeste li sigurni da "		\
		"zelite izaci iz "		\
		"programa?\n\n"			\
		"[Da / Ne]: "

#define MSG_NO_EDIT "Nema kontakata za uredivanje!\n"
#endif /* HAS_WCHAR_H */

#endif /* MESSAGES_MAIN_H */
