#ifndef MESSAGES_CONTACT_H
#define MESSAGES_CONTACT_H 1

#include "definitions/int.h"

#define MSG_CONTACT_FNAME_NEW "Novo ime: "
#define MSG_CONTACT_LNAME_NEW "Novo prezime: "
#define MSG_CONTACT_NUM_NEW "Novi broj mobitela: "
#define MSG_CONTACT_MAIL_NEW "Novi E-Mail: "
#define MSG_CONTACT_GENDER_NEW "Novi spol: "
#define MSG_WRONG_CHOICE "Krivi odabir!\n"
#define MSG_CURRENT_INDEX "\nTrenutni indeks: " PR_U32

#ifdef HAS_WCHAR_H
#define MSG_MENU_PRINT L"Unesite:\n\n"				\
			L"1) Ispis svih kontakata\n"		\
			L"2) Ispis po imenu\n"			\
			L"3) Ispis po prezimenu\n"		\
			L"4) Ispis po dobi\n"			\
			L"5) Ispis po spolu\n"			\
			L"6) Ispis po broju mobitela\n\n"

#define MSG_CONTACT_NOT_FOUND L"Nema kontakata koji odgovaraju tra"	\
				L"\u017e" L"enom pojmu!\n"

#define MSG_CONTACT_DATE_NEW L"Novi datum ro" L"\u0111" L"enja: "
#define MSG_CONTACT_DELETED L"Kontakt %ls %ls je obrisan!\n"
#define MSG_CONTACT_ADD L"Koliko kontakata " L"\u017e" L"elite unijeti? "
#define MSG_CONTACT_MAIL_INPUT L"Unesite E-Mail " PR_U32 L". kontakta: "
#define MSG_CONTACT_FNAME_INPUT L"\nUnesite ime " PR_U32 L". kontakta: "
#define MSG_CONTACT_LNAME_INPUT L"Unesite prezime " PR_U32 L". kontakta: "
#define MSG_CONTACT_NUM_INPUT L"Unesite broj mobitela " PR_U32 L"."	\
				L" kontakta [+385XXXXXXXXX]: "

#define MSG_CONTACT_GENDER_INPUT L"Unesite spol " PR_U32	\
				L". kontakta [M / " L"\u017d" L"]: "

#define MSG_CONTACT_DATE_INPUT L"Unesite datum ro" L"\u0111" L"enja "	\
				PR_U32 L". kontakta [DD/MM/GGGG]: "

#define MSG_CONTACT_SEARCH L"Unesite odabrani atribut za "	\
			L"pretra" L"\u017e" L"ivanje: "

#define MSG_CONTACT_PRINT_ALL L"Indeks: " PR_U32			\
				L"\nIme: " PR_STR			\
				L"\nPrezime: " PR_STR			\
				L"\nSpol: " PR_STR			\
				L"\nDob: " PR_STR			\
				L"\nE-Mail: " PR_STR			\
				L"\nBroj mobitela: " PR_STR		\
				L"\nDatum ro" L"\u0111" L"enja: "	\
				PR_U16 L"/" PR_U16 L"/" PR_U16		\
				L"\n\n"

#define MSG_CONTACT_EDIT L"Unesite indekse kontakata koje"		\
			L" \u017e" L"elite urediti\n(vi" L"\u0161"	\
			L"e unosa odvojiti zarezom ili razmakom): "

#define MSG_CONTACT_DEL L"Unesite indekse kontakata koje"		\
			L" \u017e" L"elite obrisati\n(vi" L"\u0161"	\
			L"e unosa odvojiti zarezom ili razmakom): "

#define MSG_CONTACT_FNAME_CURRENT L"Trenutno ime: %ls\n"
#define MSG_CONTACT_LNAME_CURRENT L"Trenutno prezime: %ls\n"
#define MSG_CONTACT_NUM_CURRENT L"Trenutni broj mobitela: %ls\n"
#define MSG_CONTACT_MAIL_CURRENT L"Trenutni E-Mail: %ls\n"
#define MSG_CONTACT_GENDER_CURRENT L"Trenutni spol: %ls\n"
#define MSG_CONTACT_DATE_CURRENT L"Trenutni datum ro" L"\u0111" L"enja: "	\
				PR_U16 L"/" PR_U16 L"/" PR_U16 L"\n"
#else
#define MSG_MENU_PRINT "Unesite:\n\n"				\
			"1) Ispis svih kontakata\n"		\
			"2) Ispis po imenu\n"			\
			"3) Ispis po prezimenu\n"		\
			"4) Ispis po dobi\n"			\
			"5) Ispis po spolu\n"			\
			"6) Ispis po broju mobitela\n\n"

#define MSG_CONTACT_DATE_NEW "Novi datum rodenja: "
#define MSG_CONTACT_NOT_FOUND "Nema kontakata koji odgovaraju trazenom pojmu!\n"
#define MSG_CONTACT_ADD "Koliko kontakata zelite unijeti? "
#define MSG_CONTACT_MAIL_INPUT "Unesite E-Mail " PR_U32 ". kontakta: "
#define MSG_CONTACT_FNAME_INPUT "\nUnesite ime " PR_U32 ". kontakta: "
#define MSG_CONTACT_LNAME_INPUT "Unesite prezime " PR_U32 ". kontakta: "
#define MSG_CONTACT_SEARCH "Unesite odabrani atribut za pretrazivanje: "

#define MSG_CONTACT_EDIT "Unesite indekse kontakata koje\n"	\
			"zelite urediti (vise unosa\n"		\
			"odvojiti zarezom bez razmaka): "

#define MSG_CONTACT_DEL "Unesite indekse kontakata koje"		\
			"zelite obrisati\n(vise unosa "			\
			"odvojiti zarezom ili razmakom): "

#define MSG_CONTACT_DELETED "Kontakt %s %s je obrisan!\n"
#define MSG_CONTACT_NUM_INPUT "Unesite broj mobitela " PR_U32 "."	\
				" kontakta [+385XXXXXXXXX]: "

#define MSG_CONTACT_GENDER_INPUT "Unesite spol " PR_U32		\
				". kontakta [M / Z]: "

#define MSG_CONTACT_DATE_INPUT "Unesite datum rodenja " PR_U32	\
				". kontakta [DD/MM/GGGG] :"

#define MSG_CONTACT_PRINT_ALL "Indeks: " PR_U32			\
				"\nIme: " PR_STR		\
				"\nPrezime: " PR_STR		\
				"\nSpol: " PR_STR		\
				"\nDob: " PR_STR		\
				"\nE-Mail: " PR_STR		\
				"\nBroj mobitela: " PR_STR	\
				"\nDatum rodenja: "		\
				PR_U16 "/" PR_U16 "/" PR_U16	\
				"\n\n"

#define MSG_CONTACT_FNAME_CURRENT "Trenutno ime: %s\n"
#define MSG_CONTACT_LNAME_CURRENT "Trenutno prezime: %s\n"
#define MSG_CONTACT_NUM_CURRENT "Trenutni broj mobitela: %s\n"
#define MSG_CONTACT_MAIL_CURRENT "Trenutni E-Mail: %s\n"
#define MSG_CONTACT_GENDER_CURRENT "Trenutni spol: %s\n"
#define MSG_CONTACT_DATE_CURRENT "Trenutni datum rodenja: "	\
				PR_U16 "/" PR_U16 "/" PR_U16 "\n"
#endif /* HAS_WCHAR_H */

#endif /* MESSAGES_CONTACT_H */
