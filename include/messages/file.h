#ifndef MESSAGES_FILE_H
#define MESSAGES_FILE_H 1

#define MSG_ERR_FILE_READ "error: Failed to open file for reading!\n"
#define MSG_ERR_FILE_FREAD "error: Failed to read from file!\n"
#define MSG_ERR_FILE_WRITE "error: Failed to create file!\n"
#define MSG_ERR_FILE_FWRITE "error: Failed to write to file!\n"

#endif /* MESSAGES_FILE_H */
