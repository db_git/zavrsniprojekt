#ifndef MESSAGES_INPUT_H
#define MESSAGES_INPUT_H 1

#define MSG_ERR_FGETS "error: failed to read input\n"
#define MSG_ERR_STRTOUL "error: failed to convert string to number\n"

#endif /* MESSAGES_INPUT_H */
