#ifndef MESSAGES_VCARD_H
#define MESSAGES_VCARD_H 1

#include "definitions/wide.h"

#ifdef HAS_WCHAR_H
#define MSG_EXPORT L"Kontakti uspje" L"\u0161" L"no izvezeni.\n"	\
			L"Putanja do datoteke: " WIDE(DATA_DIR)	\
			L"/export.vcf\n"
#define MSG_ERR_EXPORT L"Neuspje" L"\u0161" L"an izvoz kontakata!\n"
#else
#define MSG_EXPORT "Kontakti uspjesno izvezeni.\n"		\
			"Putanja do datoteke: " DATA_DIR	\
			"/export.vcf\n"
#define MSG_ERR_EXPORT "Neuspjesan izvoz kontakata!\n"
#endif /* HAS_WCHAR_H */

#endif /* MESSAGES_VCARD_H */
