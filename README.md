## **Završni projekt iz kolegija Programiranje 2**

### _Program za upravljanje kontaktima_

#### Program omogućava dodavanje novih kontakata, brisanje postojećih te njihovo uređivanje. Mogu se ispisati svi kontakti ili po određenom kriteriju te sortirati. Kontakti se učitavaju iz datoteke prilikom pokretanja programa te se automatski spremaju. Izvoz kontakata u .vcf formatu.

---

### Linux/BSD
```
mkdir build && cd build
cmake .. -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release
make
```

### Mac OS
```
mkdir build && cd build
cmake .. -G "Xcode"
```

### Windows
```
mkdir build && cd build
cmake .. -A x64 -G "Visual Studio 14 2015"
```
