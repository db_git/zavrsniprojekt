/*
 *	ZavrsniProjekt
 *	Copyright (C) 2020  Denis Bošnjaković
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cmakeconf.h>
#include <errno.h>
#include <stddef.h>

#ifdef HAS_INTTYPES_H
#include <stdint.h>
#else
#include <limits.h>
#endif /* HAS_INTTYPES_H */
#ifdef HAS_WCHAR_H
#include <wctype.h>
#else
#include <ctype.h>
#endif /* HAS_WCHAR_H */

#include "definitions/bool.h"
#include "definitions/char.h"
#include "definitions/define.h"
#include "definitions/os.h"
#include "definitions/int.h"
#include "functions/std/strtoul.h"

#ifdef HAS_INTTYPES_H
#define UNSIGNED_LIMIT UINT_FAST64_MAX
#else
#if (OS_TYPE == 32)
#define UNSIGNED_LIMIT ULLONG_MAX
#else
#define UNSIGNED_LIMIT ULONG_MAX
#endif /* OS_TYPE */
#endif /* HAS_INTTYPES_H */

/* Simple reimplementation of standard library function strtoul */
int_u64_t
str_uint(char_t * restrict_ptr const string,
	char_t * restrict_ptr * restrict_ptr const endptr)
{
	const char_t *sp = string;
	int_t c = 0;
	int_u64_t target = 0;
	bool_t conversion = FALSE_DEF;
	int_u64_t limit_num = UNSIGNED_LIMIT / 10;
	int_t limit_digit = UNSIGNED_LIMIT % 10;

	do {
		c = (int_t) *sp++;
	} while(!ISALNUM_DEF(c));

	for (;; c = (int_t) *sp++) {
		if (c >= '0' && c <= '9') c -= '0';
		else break;

		if (target == limit_num && c > limit_digit) {
			conversion = FALSE_DEF;
			errno = ERANGE;
			target = UNSIGNED_LIMIT;
			break;
		} else {
			conversion = TRUE_DEF;
			target *= 10;
			target += (int_u64_t) c;
		}
	}

	if(endptr) *endptr = conversion ? string - 1 : NULL;
	return target;
}
