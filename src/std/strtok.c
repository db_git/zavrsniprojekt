/*
 *	ZavrsniProjekt
 *	Copyright (C) 2020  Denis Bošnjaković
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cmakeconf.h>
#include <stddef.h>

#ifdef HAS_WCHAR_H
#include <wchar.h>
#else
#include <string.h>
#endif /* HAS_WCHAR_H */

#include "definitions/char.h"
#include "definitions/define.h"
#include "definitions/restrict.h"
#include "functions/std/strtok.h"

/* Simple reimplementation of standard library function strtok */
char_t *
str_token(
	register char_t * restrict_ptr str,
	register const char_t * restrict_ptr const delims,
	register char_t * restrict_ptr * restrict_ptr const state)
{
	char_t *temp = NULL;

	if (!str) str = *state;
	if (!*str) return NULL;

	temp = str + STRCSPN_DEF(str, delims);
	*state = temp + STRSPN_DEF(temp, delims);

	*temp = '\0';
	return str;
}
