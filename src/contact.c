/*
 *	ZavrsniProjekt
 *	Copyright (C) 2020  Denis Bošnjaković
 *
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if defined(_MSC_VER)		\
 || defined(_MSC_FULL_VER)	\
 || defined(_MSC_BUILD)
#define _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_DEPRECATE
#define _CRT_NONSTDC_NO_DEPRECATE
#endif /* _MSC_VER, _MSC_FULL_VER, _MSC_BUILD */

#include <cmakeconf.h>
#include <errno.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#ifdef HAS_WCHAR_H
#include <wchar.h>
#include <wctype.h>
#else
#include <ctype.h>
#include <string.h>
#endif /* HAS_WCHAR_H */

#include "definitions/bool.h"
#include "definitions/char.h"
#include "definitions/define.h"
#include "definitions/inline.h"
#include "definitions/int.h"
#include "definitions/restrict.h"
#include "definitions/struct.h"
#include "definitions/wide.h"
#include "functions/contact.h"
#include "functions/input.h"
#include "functions/memory.h"
#include "functions/terminal.h"
#include "functions/std/strtok.h"
#include "functions/std/strtoul.h"
#include "messages/contact.h"
#include "messages/print.h"

enum Print {
	ALL = 1,
	FNAME,
	LNAME,
	AGE,
	GENDER,
	PHONE
};

static int name_sanitizer(char_t * restrict_ptr);
static int phone_sanitizer(const char_t * restrict_ptr);
static int gender_sanitizer(char_t * restrict_ptr const);
static int age_sanitizer(char_t * restrict_ptr);
static int email_sanitizer(const char_t * restrict_ptr const);
static int date_sanitizer(char_t * restrict_ptr,
			struct Kontakt * const restrict_ptr * restrict_ptr);

static int validate_strtok_string(char_t * restrict_ptr);
inline_func static void tokenize(int_u32_t * restrict_ptr const,
			int_u32_t * restrict_ptr const,
			const int_u32_t, const char_t * restrict_ptr const);

inline_func static int_u32_t sort(int_u32_t * const, int_s64_t);
inline_func static int date_to_age(
			struct Kontakt * const restrict_ptr * restrict_ptr,
			const int_u16_t * restrict_ptr);

inline_func static void unos_kontakta(const int_u32_t, const int_u32_t,
			struct Kontakt * restrict_ptr const * restrict_ptr);

inline_func static struct Kontakt **delete_contact(struct Kontakt **,
			int_u32_t * restrict_ptr const, const int_u32_t);

/* Change first letter to uppercase, only letters are allowed */
static int
name_sanitizer(char_t * restrict_ptr name)
{
	register size_t slen = STRLEN_DEF(name);

	if (!ISALPHA_DEF((int_t) *name)) return 1;
	else *name = (char_t) TOUPPER_DEF((int_t) *name);

	for (++name; --slen; ++name) {
		if (!ISALPHA_DEF((int_t) *name)) return 1;
#ifdef HAS_WCHAR_H
		/* Last character in Latin Extended-A Unicode table */
		else if (*name > 0x17F) return 1;
#endif /* HAS_WCHAR_H */
		else *name = (char_t) TOLOWER_DEF((int_t) *name);
	}
	return 0;
}

/* Check first character for plus sign, only numbers are allowed */
static int
phone_sanitizer(const char_t * restrict_ptr number)
{
	register size_t slen = STRLEN_DEF(number);

	if (*number != '+') return 1;

	for (++number; --slen; ++number)
		if (!ISDIGIT_DEF((int_t) *number)) return 1;

	return 0;
}

/* Only two genders for now */
static int
gender_sanitizer(char_t * restrict_ptr const gender)
{
	*gender = (char_t) TOUPPER_DEF((int_t) *gender);
#ifdef HAS_WCHAR_H
	return (wcsncmp(gender, L"M", 1) && wcsncmp(gender, L"\u017d", 1));
#else
	return (strncmp(gender, "M", 1) && strncmp(gender, "Z", 1));
#endif /* HAS_WCHAR_H */
}

/* Check if number is entered */
static int
age_sanitizer(char_t * restrict_ptr age)
{
	register size_t slen = STRLEN_DEF(age);

	for (; --slen; ++age)
		if (!ISDIGIT_DEF((int_t) *age)) return 1;

	return 0;
}

/*
 * Make sure correct email format is entered. Simple checking only,
 * no checking for quoted local part or other allowed characters
 */
static int
email_sanitizer(const char_t * restrict_ptr const email)
{
	register size_t i = 0, j = 0;
	register const size_t slen = STRLEN_DEF(email);

	/* Only numbers and ASCII letters are allowed */
	for (i = 0; i < slen; ++i) {
		if (*(email + i) == '@') continue;
		else if (*(email + i) == '.') continue;
		else if (!ISALNUM_DEF((int_t) *(email + i))) return 1;
		else if(ISSPACE_DEF((int_t) *(email + i))) return 1;
#ifdef HAS_WCHAR_H
		/* Last character in Basic Latin Unicode table */
		else if (*(email + i) > 0x7F) return 1;
#endif /* HAS_WCHAR_H */
	}

	/* Can't start or end with dot */
	if(*email == '.') return 1;
	else if (*(email + (slen - 1)) == '.') return 1;

	/* Can't have consecutive dots */
	for (i = 0; i < slen - 1; i++) {
		if(*(email + i) == '.')
			if (*(email + i + 1) == '.')
				return 1;
	}

	/* Find first occurrence of @ sign */
	for (i = 1; i < slen; ++i)
		if (*(email + i) == '@') break;

	/* Can't have multiple @ signs */
	for (j = i + 1; j < slen; ++j)
		if (*(email + j) == '@') return 1;

	/* Local part must not exceed 64 characters */
	if (i > 64) return 1;
	/* Domain part must not exceed 63 characters */
	else if((slen - (i + 1)) > 63) return 1;
	/* Last character must be letter */
	else if (!ISALPHA_DEF((int_t) *(email + (slen - 1)))) return 1;

	/* Must have . atleast 1 character after @ */
	for (j = i + 2; j < slen; ++j)
		if (*(email + j) == '.') return 0;
	return 1;
}

/* Convert date to age */
inline_func static int
date_to_age(struct Kontakt * const restrict_ptr * restrict_ptr ptr,
				const int_u16_t * restrict_ptr datum)
{
	time_t curr_time;
	struct tm *time_s;
	struct Datum birth = { 0 }, current = { 0 };
	int_u16_t age = 0;
	int_u16_t dani[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

	time(&curr_time);
	time_s = localtime(&curr_time);
	current.gggg = (int_u16_t) time_s->tm_year + 1900;

	birth.dd = *datum++;
	birth.mm = *datum++;
	birth.gggg = *datum;

	if (birth.dd < 1) return 1;
	else if (birth.mm > 12 || birth.mm < 1) return 1;
	else if (birth.gggg < 1900 || birth.gggg > current.gggg) return 1;

	if (!(birth.gggg % 400) || (!(birth.gggg % 4)
	&& birth.gggg % 100)) dani[1] = 29;
	if (birth.dd > *(dani + (birth.mm - 1))) return 1;

	(*ptr)->datum_rodjenja = birth;
	current.dd = (int_u16_t) time_s->tm_mday;
	current.mm = (int_u16_t) time_s->tm_mon + 1;

	age = current.gggg - birth.gggg;
	if (birth.mm > current.mm) --age;
	else if (birth.mm == current.mm && birth.dd > current.dd) --age;

	SPRINTF_DEF((*ptr)->godine, AGE_LENGTH, PR_U16, age);
	return 0;
}

/* Check correct date format */
static int
date_sanitizer(char_t * restrict_ptr date,
		struct Kontakt * const restrict_ptr * restrict_ptr ptr)
{
	int_u16_t datum[3];
	char_t *state, *token;
	register size_t i = 0, brojevi = 0, znakovi = 0;
	register size_t slen = STRLEN_DEF(date);

	for (; slen--; ++date) {
		if (ISDIGIT_DEF((int_t) *date)) brojevi++;
		else if (*date == '/') znakovi++;
		else return 1;
	}
	if (brojevi < 6) return 1;
	else if (znakovi != 2) return 1;

	date -= (brojevi + znakovi);
	token = str_token(date, WIDE("/"), &state);
	while (token) {
		*(datum + i++) = (int_u16_t) str_uint(token, NULL);
		token = str_token(NULL, WIDE("/"), &state);
	}

	return date_to_age(ptr, datum);
}

/* Helper function that adds new contacts */
inline_func static void
unos_kontakta(const int_u32_t stari_broj, const int_u32_t novi_broj,
		struct Kontakt * restrict_ptr const * restrict_ptr ptr)
{
	register int_u32_t redni_broj = stari_broj + 1;
	register struct Kontakt * restrict_ptr const
	* restrict_ptr const endptr = ptr + novi_broj;
	char_t temp[DATE_LENGTH] = { '\0' };

	for (ptr += stari_broj; ptr < endptr; ++redni_broj, ++ptr) {
		PRINT(MSG_CONTACT_FNAME_INPUT, redni_broj);
		do {
			get_string_input((*ptr)->ime, FNAME_LENGTH);
		} while(name_sanitizer((*ptr)->ime));

		PRINT(MSG_CONTACT_LNAME_INPUT, redni_broj);
		do {
			get_string_input((*ptr)->prezime, LNAME_LENGTH);
		} while(name_sanitizer((*ptr)->prezime));

		PRINT(MSG_CONTACT_NUM_INPUT, redni_broj);
		do {
			get_string_input((*ptr)->broj, PHONE_LENGTH);
		} while(STRLEN_DEF((*ptr)->broj) != 13
		|| phone_sanitizer((*ptr)->broj));

		PRINT(MSG_CONTACT_MAIL_INPUT, redni_broj);
		do {
			get_string_input((*ptr)->email, EMAIL_LENGTH);
		} while(email_sanitizer((*ptr)->email));

		PRINT(MSG_CONTACT_GENDER_INPUT, redni_broj);
		do {
			get_string_input((*ptr)->spol, GENDER_LENGTH);
		} while(gender_sanitizer((*ptr)->spol));

		PRINT(MSG_CONTACT_DATE_INPUT, redni_broj);
		do {
			get_string_input(temp, DATE_LENGTH);
		} while(date_sanitizer(temp, ptr));
	}
}

/* Verify string before trying to split it */
static int
validate_strtok_string(char_t * restrict_ptr str)
{
	register size_t slen = STRLEN_DEF(str);

	if (*str == ',') return 1;
	else if (*(str + (slen - 1)) == ',') return 1;

	for (; --slen; ++str) {
		if (*str == ',') continue;
		else if (*str == ' ') continue;
		else if (!ISDIGIT_DEF((int_t) *str)) return 1;
	}

	return 0;
}

/* Actual contact deletion */
inline_func static struct Kontakt **
delete_contact(struct Kontakt **polje_kontakti,
		int_u32_t * restrict_ptr const broj_kontakata,
		const int_u32_t odabir)
{
	register int_u32_t i = 0;
	struct Kontakt **temp_array = NULL;

	PRINT(MSG_CONTACT_DELETED,
	(*(polje_kontakti + odabir))->ime,
	(*(polje_kontakti + odabir))->prezime);

	/* Free whole array if deleting the only contact in it */
	if ((*broj_kontakata)-- < 2) {
		struct_dealloc(&polje_kontakti, 1);
		return NULL;
	}

	sfree((void *) &*(polje_kontakti + odabir), 1, sizeof **polje_kontakti);
	for (i = odabir; i < *broj_kontakata; ++i)
		*(polje_kontakti + i) = *(polje_kontakti + (i + 1));

	return (temp_array = srealloc(polje_kontakti,
	*broj_kontakata, 0, sizeof *polje_kontakti));
}

/* Split string by delimiters and convert each substring to number */
inline_func static void
tokenize(
	int_u32_t * restrict_ptr const indeksi,
	int_u32_t * restrict_ptr const uneseno,
	const int_u32_t broj_kontakata,
	const char_t * restrict_ptr const delims)
{
	char_t * restrict_ptr token, *state;
	char_t *unos = scalloc(broj_kontakata << 3, sizeof *unos);

	do {
		get_string_input(unos, broj_kontakata << 3);
	} while(validate_strtok_string(unos));

	token = str_token(unos, delims, &state);
	while (token) {
		*(indeksi + (*uneseno)++) = (int_u32_t) str_uint(token, NULL);
		token = str_token(NULL, delims, &state);
		if (*uneseno >= (broj_kontakata << 2)) break;
	}

	sfree((void *) &unos, broj_kontakata << 3, sizeof *unos);
	return;
}

/* Sort and remove duplicate indexes */
inline_func static int_u32_t
sort(int_u32_t * const polje, int_s64_t elementi)
{
	register int_u32_t tmp = 0, tmp_elementi = (int_u32_t) elementi - 1;
	register int_s64_t razmak = 0, i = 0, j = 0;

	for (razmak = elementi >> 1; razmak > 0; --razmak) {
		for (i = razmak; i < elementi; ++i)
			for (j = i - razmak; j >= 0 && *(polje + j) >
			*(polje + (j + razmak)); j -= razmak) {
				tmp = *(polje + j);
				*(polje + j) = *(polje + (j + razmak));
				*(polje + (j + razmak)) = tmp;
			}
	}

	for (tmp = 0; tmp < tmp_elementi; ++tmp) {
		if (*(polje + tmp) == *(polje + (tmp + 1))) {
			for (i = 0; i < elementi - 1; ++i)
				*(polje + i) = *(polje + (i + 1));
			--elementi;
		}
	}

	return (int_u32_t) elementi;
}

/* Add new contacts */
void
dodaj_kontakt(
	struct Kontakt ** restrict_ptr * restrict_ptr const polje_kontakti,
	int_u32_t * restrict_ptr const broj_kontakata)
{
	register int_u32_t i = 0;
	register int_u32_t stari_broj_kontakata = 0, novi_broj_kontakata = 0;
	struct Kontakt **temp_array = NULL;

	do {
		PRINT(PR_STR, MSG_CONTACT_ADD);
		novi_broj_kontakata = get_uint_input(25);
	} while(novi_broj_kontakata < 1);

	if (*polje_kontakti) {
		stari_broj_kontakata = *broj_kontakata;

		temp_array = srealloc(*polje_kontakti, stari_broj_kontakata,
		novi_broj_kontakata, sizeof **polje_kontakti);

		*broj_kontakata += novi_broj_kontakata;
		for (i = stari_broj_kontakata; i < *broj_kontakata; ++i)
			*(temp_array + i) = scalloc(1, sizeof **temp_array);
	} else {
		stari_broj_kontakata = 0;
		*broj_kontakata = novi_broj_kontakata;
		temp_array = struct_alloc(*broj_kontakata);
	}
	unos_kontakta(stari_broj_kontakata, *broj_kontakata, temp_array);
	*polje_kontakti = temp_array;
	return;
}

/* Print contacts */
void
ispis_kontakata(
	struct Kontakt * restrict_ptr const * restrict_ptr const polje_kontakti,
	const int_u32_t broj_kontakata)
{
	int_u8_t odabir = 0;
	register int_u32_t i = 0;
	const struct Kontakt * restrict_ptr ptr = NULL;

	clear_terminal();
	PRINT(PR_STR, MSG_MENU_PRINT);
	do {
		odabir = (int_u8_t) get_uint_input(3);
	} while(odabir < ALL || odabir > PHONE);

	clear_terminal();
	if (odabir < FNAME) {
		for (i = 0; i < broj_kontakata; ++i) {
			ptr = *(polje_kontakti + i);
			PRINT(MSG_CONTACT_PRINT_ALL, i,
			ptr->ime, ptr->prezime, ptr->spol,
			ptr->godine, ptr->email, ptr->broj,
			ptr->datum_rodjenja.dd, ptr->datum_rodjenja.mm,
			ptr->datum_rodjenja.gggg);
		}
	} else {
		bool_t found = FALSE_DEF;
		char_t search[LNAME_LENGTH];
		register size_t slen = 0;
		register const char_t * restrict_ptr str = NULL;

		PRINT(PR_STR, MSG_CONTACT_SEARCH);
		switch (odabir) {
		case FNAME:
			do {
				get_string_input(search, FNAME_LENGTH);
			} while(name_sanitizer(search));
			break;
		case LNAME:
			do {
				get_string_input(search, LNAME_LENGTH);
			} while(name_sanitizer(search));
			break;
		case AGE:
			do {
				get_string_input(search, AGE_LENGTH);
			} while(age_sanitizer(search));
			break;
		case GENDER:
			do {
				get_string_input(search, GENDER_LENGTH);
			} while(gender_sanitizer(search));
			break;
		case PHONE:
			do {
				get_string_input(search, PHONE_LENGTH);
			} while(phone_sanitizer(search));
			break;
		default:
			PRINT_ERR(PR_STR, WIDE(MSG_WRONG_CHOICE));
			exit(-EINVAL);
		}

		slen = STRLEN_DEF(search);
		for (i = 0; i < broj_kontakata; ++i) {
			ptr = *(polje_kontakti + i);
			switch (odabir) {
			case FNAME: str = ptr->ime; break;
			case LNAME: str = ptr->prezime; break;
			case AGE: str = ptr->godine; break;
			case GENDER: str = ptr->spol; break;
			case PHONE: str = ptr->broj; break;
			default:
				PRINT_ERR(PR_STR, WIDE(MSG_WRONG_CHOICE));
				exit(-EINVAL);
			}

			if (!STRNCMP_DEF(str, search, slen)) {
				found = TRUE_DEF;
				PRINT(MSG_CONTACT_PRINT_ALL, i,
				ptr->ime, ptr->prezime, ptr->spol,
				ptr->godine, ptr->email, ptr->broj,
				ptr->datum_rodjenja.dd, ptr->datum_rodjenja.mm,
				ptr->datum_rodjenja.gggg);
			}
		}

		if (!found) PRINT(PR_STR, MSG_CONTACT_NOT_FOUND);
	}
	return;
}

/* Edit contact based on entered array index */
void
uredi_kontakt(
	struct Kontakt * restrict_ptr const * restrict_ptr const polje_kontakti,
	const int_u32_t broj_kontakata)
{
	register int_u32_t odabir = 0, i = 0;
	int_u32_t uneseno_indeksa = 0;
	int_u32_t *indeksi = scalloc(broj_kontakata << 2, sizeof *indeksi);
	struct Kontakt * restrict_ptr ptr = NULL;
	char_t date[DATE_LENGTH] = { '\0' };

	PRINT(PR_STR, MSG_CONTACT_EDIT);
	tokenize(indeksi, &uneseno_indeksa, broj_kontakata, WIDE(", "));

	uneseno_indeksa = sort(indeksi, (int_s64_t) uneseno_indeksa);
	for (i = 0; i < uneseno_indeksa; ++i) {
		if ((odabir = *(indeksi + i)) >= broj_kontakata) continue;
		ptr = *(polje_kontakti + odabir);
		PRINT(WIDE(MSG_CURRENT_INDEX) WIDE("\n"), odabir);

		PRINT(MSG_CONTACT_FNAME_CURRENT, ptr->ime);
		PRINT(WIDE(MSG_CONTACT_FNAME_NEW));
		do {
			get_string_input(ptr->ime, FNAME_LENGTH);
		} while(name_sanitizer(ptr->ime));

		PRINT(MSG_CONTACT_LNAME_CURRENT, ptr->prezime);
		PRINT(WIDE(MSG_CONTACT_LNAME_NEW));
		do {
			get_string_input(ptr->prezime, LNAME_LENGTH);
		} while(name_sanitizer(ptr->prezime));

		PRINT(MSG_CONTACT_NUM_CURRENT, ptr->broj);
		PRINT(WIDE(MSG_CONTACT_NUM_NEW));
		do {
			get_string_input(ptr->broj, PHONE_LENGTH);
		} while(STRLEN_DEF(ptr->broj) != 13
		|| phone_sanitizer(ptr->broj));

		PRINT(MSG_CONTACT_MAIL_CURRENT, ptr->email);
		PRINT(WIDE(MSG_CONTACT_MAIL_NEW));
		do {
			get_string_input(ptr->email, EMAIL_LENGTH);
		} while(email_sanitizer(ptr->email));

		PRINT(MSG_CONTACT_GENDER_CURRENT, ptr->spol);
		PRINT(WIDE(MSG_CONTACT_GENDER_NEW));
		do {
			get_string_input(ptr->spol, GENDER_LENGTH);
		} while(gender_sanitizer(ptr->spol));

		PRINT(MSG_CONTACT_DATE_CURRENT, ptr->datum_rodjenja.dd,
		ptr->datum_rodjenja.mm, ptr->datum_rodjenja.gggg);
		PRINT(MSG_CONTACT_DATE_NEW);
		do {
			get_string_input(date, DATE_LENGTH);
		} while(date_sanitizer(date, &ptr));
	}

	sfree((void *) &indeksi, broj_kontakata << 2, sizeof *indeksi);
	return;
}

/* Delete contact based on entered array index */
struct Kontakt **
obrisi_kontakt(struct Kontakt ** restrict_ptr polje_kontakti,
		int_u32_t * restrict_ptr const broj_kontakata)
{
	register int_u32_t odabir = 0, i = 0, j = 0;
	int_u32_t uneseno_indeksa = 0;
	int_u32_t *indeksi = scalloc((*broj_kontakata) << 2, sizeof *indeksi);

	PRINT(PR_STR, MSG_CONTACT_DEL);
	tokenize(indeksi, &uneseno_indeksa, *broj_kontakata, WIDE(", "));

	PRINT(WIDE("\n"));
	uneseno_indeksa = sort(indeksi, (int_s64_t) uneseno_indeksa);
	for (i = 0; i < uneseno_indeksa; ++i) {
		if (!polje_kontakti) break;
		if ((odabir = *(indeksi + i)) >= *broj_kontakata) continue;

		polje_kontakti = delete_contact(polje_kontakti,
		broj_kontakata, odabir);
		for (j = i; j < uneseno_indeksa; ++j) --*(indeksi + j);
	}

	sfree((void *) &indeksi, (*broj_kontakata) << 2, sizeof *indeksi);
	return polje_kontakti;
}
